## Introduction

Ansible Tower is a central system for Ansible automation scheduling and inventory management, with robust RBAC and many other useful features for managing automation tasks for teams. AWX is the open source upstream project where Tower's bleeding-edge development takes place. The relationship between the two is similar to Fedora (the less stable open source upstream which requires no license to use) and Red Hat Enterprise Linux (which is more stable and requires a license).

## Installing AWX on kubernetes

```
kubectl create -f kubernetes-awx.yml
```

The tower.yml can be changed example the hostname, images or the password

```
kubectl create -f tower.yml
```